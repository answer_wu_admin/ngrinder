/* 
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */
package net.grinder;

<<<<<<< HEAD
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.io.File;
import java.lang.reflect.Field;
import java.util.Set;

import net.grinder.common.GrinderProperties;
import net.grinder.common.processidentity.AgentIdentity;
import net.grinder.util.NetworkUtil;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.ngrinder.AbstractMuliGrinderTestBase;

public class AgentControllerTest extends AbstractMuliGrinderTestBase {
	AgentControllerServerDaemon agentControllerServerDeamon;
=======
import net.grinder.common.GrinderProperties;
import net.grinder.common.processidentity.AgentIdentity;
import net.grinder.util.NetworkUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.ngrinder.AbstractMultiGrinderTestBase;

import java.util.Set;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class AgentControllerTest extends AbstractMultiGrinderTestBase {
	AgentControllerServerDaemon agentControllerServerDaemon;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	AgentControllerDaemon agentControllerDaemon;
	AgentControllerDaemon agentControllerDaemon2;
	SingleConsole console1;
	Set<AgentIdentity> allAvailableAgents;

	@Before
	public void before() {
<<<<<<< HEAD
		File file = new File(new File("."), "native_lib");
		System.setProperty("java.library.path", file.getAbsolutePath());
		// set sys_paths to null
		Field sysPathsField = null;
		try {
			sysPathsField = ClassLoader.class.getDeclaredField("sys_paths");
			sysPathsField.setAccessible(true);
			sysPathsField.set(null, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		agentControllerServerDeamon = new AgentControllerServerDaemon(getFreePort());
		agentControllerServerDeamon.start();

		agentControllerDaemon = new AgentControllerDaemon("127.0.0.1");
		agentControllerDaemon.setAgentConfig(agentConfig1);
		agentControllerDaemon.run(agentControllerServerDeamon.getPort());
		agentControllerDaemon2 = new AgentControllerDaemon("127.0.0.1");
		agentControllerDaemon2.setAgentConfig(agentConfig2);
		agentControllerDaemon2.run(agentControllerServerDeamon.getPort());
		sleep(2000);
		// Validate if all agents are well-attached.

		allAvailableAgents = agentControllerServerDeamon.getAllAvailableAgents();
=======
		final int freePort = getFreePort();
		agentControllerServerDaemon = new AgentControllerServerDaemon(freePort);
		agentControllerServerDaemon.start();
		agentConfig1.setControllerPort(freePort);
		agentControllerDaemon = new AgentControllerDaemon(agentConfig1);

		agentControllerDaemon.run();
		agentConfig2.setControllerPort(freePort);
		agentControllerDaemon2 = new AgentControllerDaemon(agentConfig2);
		agentControllerDaemon2.run();
		sleep(2000);
		// Validate if all agents are well-attached.

		allAvailableAgents = agentControllerServerDaemon.getAllAvailableAgents();
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		assertThat(allAvailableAgents.size(), is(2));
	}

	@After
	public void shutdown() {
		agentControllerDaemon.shutdown();
		agentControllerDaemon2.shutdown();
		sleep(1000);
<<<<<<< HEAD
		agentControllerServerDeamon.shutdown();
=======
		agentControllerServerDaemon.shutdown();
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		sleep(1000);

		if (console1 != null) {
			console1.shutdown();
			sleep(1000);
		}
	}

	@Test
	public void testAgentStatus() throws InterruptedException {

		// Validate all agents are well attached even after restarting agent
		// controller server
<<<<<<< HEAD
		agentControllerServerDeamon.shutdown();
		agentControllerServerDeamon = new AgentControllerServerDaemon(agentControllerServerDeamon.getPort());
		agentControllerServerDeamon.start();
		sleep(3000);

		// all agent should be re-attached
		allAvailableAgents = agentControllerServerDeamon.getAllAvailableAgents();
=======
		agentControllerServerDaemon.shutdown();
		agentControllerServerDaemon = new AgentControllerServerDaemon(agentControllerServerDaemon.getPort());
		agentControllerServerDaemon.start();
		sleep(3000);

		// all agent should be re-attached
		allAvailableAgents = agentControllerServerDaemon.getAllAvailableAgents();
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		assertThat(allAvailableAgents.size(), is(2));
	}

	@Test
	public void testStartAgent() {

		// Start Console
		console1 = new SingleConsole(getFreePort());
		console1.start();

		// Check there is no agents are attached.
		assertThat(console1.getAllAttachedAgentsCount(), is(0));

		// Make one agent connect to console1
		GrinderProperties grinderProperties = new GrinderProperties();
		grinderProperties.setInt(GrinderProperties.CONSOLE_PORT, console1.getConsolePort());
<<<<<<< HEAD
		String localHostAddress = NetworkUtil.getLocalHostAddress();
		grinderProperties.setProperty(GrinderProperties.CONSOLE_HOST, localHostAddress);
		AgentIdentity next = getAgentIdentity(allAvailableAgents, 0);
		agentControllerServerDeamon.startAgent(grinderProperties, next);
=======
		String localHostAddress = NetworkUtils.getLocalHostAddress();
		grinderProperties.setProperty(GrinderProperties.CONSOLE_HOST, localHostAddress);
		AgentIdentity next = getAgentIdentity(allAvailableAgents, 0);
		agentControllerServerDaemon.startAgent(grinderProperties, next);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		sleep(2000);
		assertThat(console1.getAllAttachedAgents().size(), is(1));

		// Shutdown agent controller and see agent is detached as well
<<<<<<< HEAD
		agentControllerServerDeamon.shutdown();
=======
		agentControllerServerDaemon.shutdown();
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		sleep(5000);
		assertThat(console1.getAllAttachedAgentsCount(), is(0));

	}

	@Test
<<<<<<< HEAD
	public void testStopAndStartAgentRepeatly() {
=======
	public void testStopAndStartAgentRepeatedly() {
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		// Get one agent
		AgentIdentity agentIdentity = getAgentIdentity(allAvailableAgents, 0);

		// Start console
		SingleConsole console1 = new SingleConsole(getFreePort());
		console1.start();

		// Start one agent
		GrinderProperties grinderProperties = new GrinderProperties();
		grinderProperties.setInt(GrinderProperties.CONSOLE_PORT, console1.getConsolePort());

<<<<<<< HEAD
		agentControllerServerDeamon.startAgent(grinderProperties, agentIdentity);
=======
		agentControllerServerDaemon.startAgent(grinderProperties, agentIdentity);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		sleep(3000);
		assertThat(console1.getAllAttachedAgentsCount(), is(1));

		// Stop that agent and see it's well disconnected
<<<<<<< HEAD
		agentControllerServerDeamon.stopAgent(agentIdentity);
=======
		agentControllerServerDaemon.stopAgent(agentIdentity);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		sleep(5000);
		assertThat(console1.getAllAttachedAgentsCount(), is(0));

		// Stop that agent and see it's well disconnected again.
		// It should be verified
<<<<<<< HEAD
		agentControllerServerDeamon.stopAgent(agentIdentity);
		sleep(5000);
		assertThat(console1.getAllAttachedAgentsCount(), is(0));

		agentControllerServerDeamon.startAgent(grinderProperties, agentIdentity);
=======
		agentControllerServerDaemon.stopAgent(agentIdentity);
		sleep(5000);
		assertThat(console1.getAllAttachedAgentsCount(), is(0));

		agentControllerServerDaemon.startAgent(grinderProperties, agentIdentity);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		sleep(5000);
		assertThat(console1.getAllAttachedAgentsCount(), is(1));
	}

	@Test
	public void testAgentControllerServerDaemonRecovery() {
		sleep(2000);
		// Connect all available agents
<<<<<<< HEAD
		Set<AgentIdentity> allAvailableAgents = agentControllerServerDeamon.getAllAvailableAgents();
=======
		Set<AgentIdentity> allAvailableAgents = agentControllerServerDaemon.getAllAvailableAgents();
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		assertThat(allAvailableAgents.size(), is(2));
		SingleConsole console1 = new SingleConsole(6372);
		console1.start();
		GrinderProperties grinderProperties = new GrinderProperties();
		grinderProperties.setInt(GrinderProperties.CONSOLE_PORT, console1.getConsolePort());

<<<<<<< HEAD
		agentControllerServerDeamon.startAgent(grinderProperties, getAgentIdentity(allAvailableAgents, 0));
		agentControllerServerDeamon.startAgent(grinderProperties, getAgentIdentity(allAvailableAgents, 1));
=======
		agentControllerServerDaemon.startAgent(grinderProperties, getAgentIdentity(allAvailableAgents, 0));
		agentControllerServerDaemon.startAgent(grinderProperties, getAgentIdentity(allAvailableAgents, 1));
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		sleep(1000);
		assertThat(console1.getAllAttachedAgents().size(), is(2));

		// Shutdown agent controller
<<<<<<< HEAD
		agentControllerServerDeamon.shutdown();
=======
		agentControllerServerDaemon.shutdown();
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		sleep(5000);

		assertThat(console1.getAllAttachedAgents().size(), is(0));

		// Then start again
<<<<<<< HEAD
		agentControllerServerDeamon = new AgentControllerServerDaemon(agentControllerServerDeamon.getPort());
		agentControllerServerDeamon.start();
		sleep(3000);

		// See the agent controller is attached automatically
		assertThat(agentControllerServerDeamon.getAllAttachedAgentsCount(), is(2));
		sleep(2000);
		allAvailableAgents = agentControllerServerDeamon.getAllAvailableAgents();

		// If we restart agents
		agentControllerServerDeamon.startAgent(grinderProperties, getAgentIdentity(allAvailableAgents, 0));
		agentControllerServerDeamon.startAgent(grinderProperties, getAgentIdentity(allAvailableAgents, 1));
=======
		agentControllerServerDaemon = new AgentControllerServerDaemon(agentControllerServerDaemon.getPort());
		agentControllerServerDaemon.start();
		sleep(3000);

		// See the agent controller is attached automatically
		assertThat(agentControllerServerDaemon.getAllAttachedAgentsCount(), is(2));
		sleep(2000);
		allAvailableAgents = agentControllerServerDaemon.getAllAvailableAgents();

		// If we restart agents
		agentControllerServerDaemon.startAgent(grinderProperties, getAgentIdentity(allAvailableAgents, 0));
		agentControllerServerDaemon.startAgent(grinderProperties, getAgentIdentity(allAvailableAgents, 1));
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		sleep(2000);

		// They should be successfully attached into the existing console.
		assertThat(console1.getAllAttachedAgents().size(), is(2));

	}
}
