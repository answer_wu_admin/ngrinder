/* 
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */
package org.ngrinder.common.util;

<<<<<<< HEAD
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

import java.util.Properties;

import org.junit.Test;

/**
 * Class description.
 * 
 * @author Mavlarn
 * @since
 */
public class PropertiesWrapperTest {

	/**
	 * Test method for
	 * {@link org.ngrinder.common.util.PropertiesWrapper#PropertiesWrapper(java.util.Properties)}.
	 */
=======
import org.junit.Test;

import java.util.Properties;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * Property Wrapper Test
 */
public class PropertiesWrapperTest {

>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	@Test
	public void testPropertiesWrapper() {
		Properties prop = new Properties();
		prop.put("key1", "1");
		prop.put("key2", "value2");
<<<<<<< HEAD
		PropertiesWrapper propWrapper = new PropertiesWrapper(prop);
=======
		PropertiesWrapper propWrapper = new PropertiesWrapper(prop, PropertiesKeyMapper.create("agent-properties.map"));
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad

		propWrapper.addProperty("key3", "3");
		propWrapper.addProperty("key4", "value4");

<<<<<<< HEAD
		int value1 = propWrapper.getPropertyInt("key1", 0);
		assertThat(value1, is(1));
		int value3 = propWrapper.getPropertyInt("key3", 0);
		assertThat(value3, is(3));
		int noValue = propWrapper.getPropertyInt("NoValueKey", 0);
		assertThat(noValue, is(0));

=======
		int value1 = propWrapper.getPropertyInt("key1");
		assertThat(value1, is(1));
		int value3 = propWrapper.getPropertyInt("key3");
		assertThat(value3, is(3));
		try {
			int noValue = propWrapper.getPropertyInt("NoValueKey");
			fail("should cause IllegalArgumentException");
		} catch (IllegalArgumentException e) {

		}
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		String value2 = propWrapper.getProperty("key2", "null");
		assertThat(value2, is("value2"));
		String value4 = propWrapper.getProperty("key4", "null");
		assertThat(value4, is("value4"));
		String nullValueStr = propWrapper.getProperty("NoValueKey", "null");
		assertThat(nullValueStr, is("null"));

<<<<<<< HEAD
		String newValue4 = propWrapper.getProperty("key4", "No value found for:{}");
		assertThat(newValue4, is("value4"));
		nullValueStr = propWrapper.getProperty("NoValueKey", "null", "No value found for:{}");
		assertThat(nullValueStr, is("null"));

		boolean boolVal = propWrapper.getPropertyBoolean("BoolKey", false);
		assertThat(boolVal, is(false));
		prop.put("BoolKey", "true");
		boolVal = propWrapper.getPropertyBoolean("BoolKey", false);
		assertThat(boolVal, is(true));
=======
		String newValue4 = propWrapper.getProperty("key4");
		assertThat(newValue4, is("value4"));
		nullValueStr = propWrapper.getProperty("NoValueKey", "null");
		assertThat(nullValueStr, is("null"));
		prop.put("BoolKey", "true");
		boolean boolVal = propWrapper.getPropertyBoolean("BoolKey");
		assertThat(boolVal, is(true));
		propWrapper.addProperty("hello_world", "wow");
		assertThat(propWrapper.getProperty("hello_world"), is("wow"));
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad

	}
}
