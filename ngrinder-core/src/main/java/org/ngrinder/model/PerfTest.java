/* 
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */
package org.ngrinder.model;

<<<<<<< HEAD
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.SortedSet;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import net.grinder.common.GrinderProperties;

=======
import com.google.gson.annotations.Expose;
import net.grinder.common.GrinderProperties;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.hibernate.annotations.Index;
import org.hibernate.annotations.Sort;
import org.hibernate.annotations.SortType;
import org.hibernate.annotations.Type;
<<<<<<< HEAD
import org.ngrinder.common.util.DateUtil;
import org.ngrinder.common.util.PathUtil;

import com.google.gson.annotations.Expose;

/**
 * Performance Test Entity. <br/>
 * 
 */
=======
import org.ngrinder.common.util.DateUtils;
import org.ngrinder.common.util.PathUtils;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.SortedSet;

import static org.ngrinder.common.util.AccessUtils.getSafe;

/**
 * Performance Test Entity.
 */

@SuppressWarnings({"JpaDataSourceORMInspection", "UnusedDeclaration", "JpaAttributeTypeInspection"})
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
@Entity
@Table(name = "PERF_TEST")
public class PerfTest extends BaseModel<PerfTest> {

<<<<<<< HEAD
	private static final int MARGIN_FOR_ABBREVIATATION = 8;
=======
	private static final int MARGIN_FOR_ABBREVIATION = 8;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad

	private static final int MAX_LONG_STRING_SIZE = 2048;

	private static final long serialVersionUID = 1369809450686098944L;

	private static final int MAX_STRING_SIZE = 2048;

<<<<<<< HEAD
	@Expose
=======
	public PerfTest() {

	}

	/**
	 * Constructor.
	 *
	 * @param createdUser crested user.
	 */
	public PerfTest(User createdUser) {
		this.setCreatedUser(createdUser);
	}

	@Expose
	@Cloneable
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	@Column(name = "name")
	private String testName;

	@Expose
<<<<<<< HEAD
=======
	@Cloneable
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	@Column(name = "tag_string")
	private String tagString;

	@Expose
<<<<<<< HEAD
=======
	@Cloneable
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	@Column(length = MAX_LONG_STRING_SIZE)
	private String description;

	@Expose
	@Enumerated(EnumType.STRING)
	@Column(name = "status")
<<<<<<< HEAD
	private Status status = Status.READY;

	@Expose
	/** ignoreSampleCount value, default to 0. */
	@Column(name = "ignore_sample_count")
	private Integer ignoreSampleCount = 0;
=======
	private Status status;

	@Expose
	@Cloneable
	/** ignoreSampleCount value, default to 0. */
	@Column(name = "ignore_sample_count")
	private Integer ignoreSampleCount;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad

	@Expose
	/** the scheduled time of this test. */
	@Column(name = "scheduled_time")
	@Index(name = "scheduled_time_index")
	private Date scheduledTime;

	@Expose
	/** the start time of this test. */
	@Column(name = "start_time")
	private Date startTime;

	@Expose
	/** the finish time of this test. */
	@Column(name = "finish_time")
	private Date finishTime;

<<<<<<< HEAD
	/** the target host to test. */
	@Expose
	@Column(name = "target_hosts")
	private String targetHosts;

	/** The send mail code. */
	@Expose
=======
	/**
	 * the target host to test.
	 */
	@Expose
	@Cloneable
	@Column(name = "target_hosts")
	private String targetHosts;

	/**
	 * The send mail code.
	 */
	@Expose
	@Cloneable
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	@Column(name = "send_mail", columnDefinition = "char(1)")
	@Type(type = "true_false")
	private Boolean sendMail;

<<<<<<< HEAD
	
	/** Use rampup or not. */
	@Expose
	@Column(name = "use_rampup", columnDefinition = "char(1)")
	@Type(type = "true_false")
	private Boolean useRampUp = false;

	/** The threshold code, R for run count; D for duration. */
	@Expose
=======

	/**
	 * Use rampUp or not.
	 */
	@Expose
	@Cloneable
	@Column(name = "use_rampup", columnDefinition = "char(1)")
	@Type(type = "true_false")
	private Boolean useRampUp;

	public RampUp getRampUpType() {
		return rampUpType;
	}

	public void setRampUpType(RampUp rampUpType) {
		this.rampUpType = rampUpType;
	}

	/**
	 * Use rampUp or not.
	 */
	@Expose
	@Cloneable
	@Column(name = "ramp_up_type")
	@Enumerated(EnumType.STRING)
	private RampUp rampUpType;


	/**
	 * The threshold code, R for run count; D for duration.
	 */
	@Expose
	@Cloneable
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	@Column(name = "threshold")
	private String threshold;

	@Expose
<<<<<<< HEAD
=======
	@Cloneable
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	@Column(name = "script_name")
	private String scriptName;

	@Expose
<<<<<<< HEAD
=======
	@Cloneable
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	@Column(name = "duration")
	private Long duration;

	@Expose
<<<<<<< HEAD
=======
	@Cloneable
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	@Column(name = "run_count")
	private Integer runCount;

	@Expose
<<<<<<< HEAD
=======
	@Cloneable
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	@Column(name = "agent_count")
	private Integer agentCount;

	@Expose
<<<<<<< HEAD
=======
	@Cloneable
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	@Column(name = "vuser_per_agent")
	private Integer vuserPerAgent;

	@Expose
<<<<<<< HEAD
=======
	@Cloneable
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	@Column(name = "processes")
	private Integer processes;

	@Expose
<<<<<<< HEAD
	@Column(name = "init_processes")
	private Integer initProcesses;

	@Expose
	@Column(name = "init_sleep_time")
	private Integer initSleepTime;
	
	@Expose
	@Column(name = "process_increment")
	private Integer processIncrement;

	@Expose
	@Column(name = "process_increment_interval")
	private Integer processIncrementInterval;

	@Expose
=======
	@Cloneable
	@Column(name = "ramp_up_init_count")
	private Integer rampUpInitCount;

	@Expose
	@Cloneable
	@Column(name = "ramp_up_init_sleep_time")
	private Integer rampUpInitSleepTime;

	@Expose
	@Cloneable
	@Column(name = "ramp_up_step")
	private Integer rampUpStep;

	@Expose
	@Cloneable
	@Column(name = "ramp_up_increment_interval")
	private Integer rampUpIncrementInterval;

	@Expose
	@Cloneable
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	@Column(name = "threads")
	private Integer threads;

	// followings are test result members
	@Expose
	@Column(name = "tests")
	private Long tests;

	@Expose
	@Column(name = "errors")
	private Long errors;

	@Expose
	@Column(name = "mean_test_time")
	private Double meanTestTime;

	@Expose
	@Column(name = "test_time_standard_deviation")
	private Double testTimeStandardDeviation;

	@Expose
	@Column(name = "tps")
	private Double tps;

	@Expose
	@Column(name = "peak_tps")
	private Double peakTps;

<<<<<<< HEAD
	/** Console port for this test. This is the identifier for console */
	@Column(name = "port")
	private Integer port = 0;
=======
	/**
	 * Console port for this test. This is the identifier for console
	 */
	@Column(name = "port")
	private Integer port;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad

	@Expose
	@Column(name = "test_error_cause")
	@Enumerated(EnumType.STRING)
<<<<<<< HEAD
	private Status testErrorCause = Status.UNKNOWN;
=======
	private Status testErrorCause;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad

	@Column(name = "distribution_path")
	/** The path used for file distribution */
	private String distributionPath;

	@Expose
	@Column(name = "progress_message", length = MAX_STRING_SIZE)
<<<<<<< HEAD
	private String progressMessage = "";

	@Column(name = "last_progress_message", length = MAX_STRING_SIZE)
	private String lastProgressMessage = "";

	@Expose
	@Column(name = "test_comment", length = MAX_STRING_SIZE)
	private String testComment = "";

	@Expose
	@Column(name = "script_revision")
	private Long scriptRevision = -1L;

	@Column(name = "stop_request", columnDefinition = "char(1)")
=======
	private String progressMessage;

	@Column(name = "last_progress_message", length = MAX_STRING_SIZE)
	private String lastProgressMessage;

	@Expose
	@Column(name = "test_comment", length = MAX_STRING_SIZE)
	private String testComment;

	@Expose
	@Column(name = "script_revision")
	private Long scriptRevision;

	@Expose
	@Column(name = "stop_request")
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	@Type(type = "true_false")
	private Boolean stopRequest;

	@Expose
<<<<<<< HEAD
	@Column(name = "region")
	private String region;

	@Column(name = "safe_distribution", columnDefinition = "char(1)")
	@Type(type = "true_false")
	private Boolean safeDistribution = false;
=======
	@Cloneable
	@Column(name = "region")
	private String region;

	@Column(name = "safe_distribution")
	@Cloneable
	@Type(type = "true_false")
	private Boolean safeDistribution;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad

	@Transient
	private String dateString;

	@Transient
	private GrinderProperties grinderProperties;

<<<<<<< HEAD
	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@JoinTable(name = "PERF_TEST_TAG", /** join column */
	joinColumns = @JoinColumn(name = "perf_test_id"), /** inverse join column */
	inverseJoinColumns = @JoinColumn(name = "tag_id"))
=======
	@ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
	@JoinTable(name = "PERF_TEST_TAG", /** join column */
			joinColumns = @JoinColumn(name = "perf_test_id"), /** inverse join column */
			inverseJoinColumns = @JoinColumn(name = "tag_id"))
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	@Sort(comparator = Tag.class, type = SortType.COMPARATOR)
	private SortedSet<Tag> tags;

	@Column(name = "running_sample", length = 9990)
	private String runningSample;

	@Column(name = "agent_stat", length = 9990)
<<<<<<< HEAD
	private String agentStatus;

	@Column(name = "monitor_stat", length = 2000)
	private String monitorStatus;

	@Expose
=======
	private String agentState;

	@Column(name = "monitor_stat", length = 2000)
	private String monitorState;

	@Expose
	@Cloneable
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	@Column(name = "sampling_interval")
	private Integer samplingInterval;

	@Expose
<<<<<<< HEAD
	@Column(name = "param")
	private String param;

	
=======
	@Cloneable
	@Column(name = "param")
	private String param;

	@PrePersist
	@PreUpdate
	public void init() {
		this.status = getSafe(this.status, Status.SAVED);
		this.agentCount = getSafe(this.agentCount);
		this.port = getSafe(this.port);
		this.processes = getSafe(this.processes, 1);
		this.threads = getSafe(this.threads, 1);
		this.scriptName = getSafe(this.scriptName, "");
		this.testName = getSafe(this.testName, "");
		this.progressMessage = getSafe(this.progressMessage, "");
		this.lastProgressMessage = getSafe(this.lastProgressMessage, "");
		this.testComment = getSafe(this.testComment, "");
		this.threshold = getSafe(this.threshold, "D");
		if (isThresholdRunCount()) {
			this.setIgnoreSampleCount(0);
		} else {
			this.ignoreSampleCount = getSafe(this.ignoreSampleCount);
		}
		this.runCount = getSafe(this.runCount);
		this.duration = getSafe(this.duration, 60000L);
		this.samplingInterval = getSafe(this.samplingInterval, 2);
		this.scriptRevision = getSafe(this.scriptRevision, -1L);
		this.param = getSafe(this.param, "");
		this.region = getSafe(this.region, "NONE");
		this.targetHosts = getSafe(this.targetHosts, "");
		this.description = getSafe(this.description, "");
		this.tagString = getSafe(this.tagString, "");
		this.vuserPerAgent = getSafe(this.vuserPerAgent, 1);
		this.safeDistribution = getSafe(this.safeDistribution, false);
		this.useRampUp = getSafe(this.useRampUp, false);
		this.rampUpInitCount = getSafe(this.rampUpInitCount, 0);
		this.rampUpStep = getSafe(this.rampUpStep, 1);
		this.rampUpInitSleepTime = getSafe(this.rampUpInitSleepTime, 0);
		this.rampUpIncrementInterval = getSafe(this.rampUpIncrementInterval, 1000);
		this.rampUpType = getSafe(this.rampUpType, RampUp.PROCESS);
	}


>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	public String getTestIdentifier() {
		return "perftest_" + getId() + "_" + getLastModifiedUser().getUserId();
	}

	/**
	 * Get total required run count. This is calculated by multiplying agent count, threads,
	 * processes, run count.
<<<<<<< HEAD
	 * 
=======
	 *
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return run count
	 */
	public long getTotalRunCount() {
		return getAgentCount() * getThreads() * getProcesses() * (long) getRunCount();
	}

	public String getTestName() {
		return testName;
	}

<<<<<<< HEAD
	@Cloneable
=======
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	public void setTestName(String testName) {
		this.testName = testName;
	}

	public Date getScheduledTime() {
		return scheduledTime;
	}

	public void setScheduledTime(Date scheduledTime) {
		this.scheduledTime = scheduledTime;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getFinishTime() {
		return finishTime;
	}

	public void setFinishTime(Date finishTime) {
		this.finishTime = finishTime;
	}

	public Integer getRunCount() {
		return runCount;
	}

<<<<<<< HEAD
	@Cloneable
=======
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	public void setRunCount(Integer runCount) {
		this.runCount = runCount;
	}

	public Long getDuration() {
		return duration;
	}

<<<<<<< HEAD
	@Cloneable
=======
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	public void setDuration(Long duration) {
		this.duration = duration;
	}

	public String getScriptName() {
		return scriptName;
	}

<<<<<<< HEAD
	@Cloneable
=======
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	public void setScriptName(String scriptName) {
		this.scriptName = scriptName;
	}

	public Integer getIgnoreSampleCount() {
		return ignoreSampleCount;
	}

<<<<<<< HEAD
	@Cloneable
=======
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	public void setIgnoreSampleCount(Integer ignoreSampleCount) {
		this.ignoreSampleCount = ignoreSampleCount;
	}

	public String getScriptNameInShort() {
<<<<<<< HEAD
		return PathUtil.getShortPath(scriptName);
	}

	public String getDescription() {
		return StringUtils.abbreviate(description, MAX_LONG_STRING_SIZE - MARGIN_FOR_ABBREVIATATION);
	}

	public String getLastModifiedDateToStr() {
		return DateUtil.dateToString(getLastModifiedDate());
	}

	@Cloneable
=======
		return PathUtils.getShortPath(scriptName);
	}

	public String getDescription() {
		return StringUtils.abbreviate(description, MAX_LONG_STRING_SIZE - MARGIN_FOR_ABBREVIATION);
	}

	public String getLastModifiedDateToStr() {
		return DateUtils.dateToString(getLastModifiedDate());
	}

>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	public void setDescription(String description) {
		this.description = description;
	}

	public String getTargetHosts() {
		return targetHosts;
	}

	/**
	 * Get ip address of target hosts. if target hosts 'a.com:1.1.1.1' add ip: '1.1.1.1' if target
	 * hosts ':1.1.1.1' add ip: '1.1.1.1' if target hosts '1.1.1.1' add ip: '1.1.1.1'
<<<<<<< HEAD
	 * 
=======
	 * if www.test.com:0:0:0:0:0:ffff:3d87:a969 add ip: '0:0:0:0:0:ffff:3d87:a969'
	 *
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return host ip list
	 */
	public List<String> getTargetHostIP() {
		List<String> targetIPList = new ArrayList<String>();
		String[] hostsList = StringUtils.split(StringUtils.trimToEmpty(targetHosts), ",");
		for (String hosts : hostsList) {
			String[] addresses = StringUtils.split(hosts, ":");
<<<<<<< HEAD
			if (addresses.length > 0) {
				targetIPList.add(addresses[addresses.length - 1]);
			} else {
				targetIPList.add(hosts);
=======
			if (addresses.length <= 2) {
				targetIPList.add(addresses[addresses.length - 1]);
			} else {
				targetIPList.add(hosts.substring(hosts.indexOf(":") + 1, hosts.length()));
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
			}
		}
		return targetIPList;
	}

<<<<<<< HEAD
	@Cloneable
	@ForceMergable
=======
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	public void setTargetHosts(String theTarget) {
		this.targetHosts = theTarget;
	}

	public String getThreshold() {
		return threshold;
	}

<<<<<<< HEAD
	public boolean isThreshholdDuration() {
		return "D".equals(getThreshold());
	}

	public boolean isThreshholdRunCount() {
=======
	public Boolean isThresholdDuration() {
		return "D".equals(getThreshold());
	}

	public Boolean isThresholdRunCount() {
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		return "R".equals(getThreshold());
	}


<<<<<<< HEAD
	@Cloneable
=======
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	public void setThreshold(String threshold) {
		this.threshold = threshold;
	}

	public void setGrinderProperties(GrinderProperties properties) {
		this.grinderProperties = properties;
	}

	public GrinderProperties getGrinderProperties() {
		return grinderProperties;
	}

	public Status getStatus() {
		return status;
	}


	public void setStatus(Status status) {
		this.status = status;
	}

	public Integer getAgentCount() {
		return agentCount;
	}


<<<<<<< HEAD
	@Cloneable
=======
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	public void setAgentCount(Integer agentCount) {
		this.agentCount = agentCount;
	}

	public Integer getVuserPerAgent() {
		return vuserPerAgent;
	}


<<<<<<< HEAD
	@Cloneable
=======
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	public void setVuserPerAgent(Integer vuserPerAgent) {
		this.vuserPerAgent = vuserPerAgent;
	}

	public Integer getProcesses() {
		return processes;
	}


<<<<<<< HEAD
	@Cloneable
=======
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	public void setProcesses(Integer processes) {
		this.processes = processes;
	}

<<<<<<< HEAD
	public Integer getInitProcesses() {
		return initProcesses;
	}


	@Cloneable
	public void setInitProcesses(Integer initProcesses) {
		this.initProcesses = initProcesses;
	}

	public Integer getInitSleepTime() {
		return initSleepTime;
	}


	@Cloneable
	public void setInitSleepTime(Integer initSleepTime) {
		this.initSleepTime = initSleepTime;
	}

	public Integer getProcessIncrement() {
		return processIncrement;
	}


	@Cloneable
	public void setProcessIncrement(Integer processIncrement) {
		this.processIncrement = processIncrement;
	}

	public Integer getProcessIncrementInterval() {
		return processIncrementInterval;
	}


	@Cloneable
	public void setProcessIncrementInterval(Integer processIncrementInterval) {
		this.processIncrementInterval = processIncrementInterval;
=======
	public Integer getRampUpInitCount() {
		return rampUpInitCount;
	}

	public void setRampUpInitCount(Integer initProcesses) {
		this.rampUpInitCount = initProcesses;
	}

	public Integer getRampUpInitSleepTime() {
		return rampUpInitSleepTime;
	}


	public void setRampUpInitSleepTime(Integer initSleepTime) {
		this.rampUpInitSleepTime = initSleepTime;
	}

	public Integer getRampUpStep() {
		return rampUpStep;
	}


	public void setRampUpStep(Integer processIncrement) {
		this.rampUpStep = processIncrement;
	}

	public Integer getRampUpIncrementInterval() {
		return rampUpIncrementInterval;
	}


	public void setRampUpIncrementInterval(Integer processIncrementInterval) {
		this.rampUpIncrementInterval = processIncrementInterval;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	public Integer getThreads() {
		return threads;
	}


<<<<<<< HEAD
	@Cloneable
=======
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	public void setThreads(Integer threads) {
		this.threads = threads;
	}

	public Long getTests() {
		return tests;
	}

	public void setTests(Long tests) {
		this.tests = tests;
	}

	public Long getErrors() {
		return errors;
	}

	public void setErrors(Long errors) {
		this.errors = errors;
	}

	public Double getMeanTestTime() {
		return meanTestTime;
	}

	public void setMeanTestTime(Double meanTestTime) {
		this.meanTestTime = meanTestTime;
	}

	public Double getTestTimeStandardDeviation() {
		return testTimeStandardDeviation;
	}

	public void setTestTimeStandardDeviation(Double testTimeStandardDeviation) {
		this.testTimeStandardDeviation = testTimeStandardDeviation;
	}

	public Double getTps() {
		return tps;
	}

	public void setTps(Double tps) {
		this.tps = tps;
	}

	public Double getPeakTps() {
		return peakTps;
	}

	public void setPeakTps(Double peakTps) {
		this.peakTps = peakTps;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public Status getTestErrorCause() {
		return testErrorCause;
	}

	public void setTestErrorCause(Status errorCause) {
		this.testErrorCause = errorCause;
	}

	public String getDistributionPath() {
		return distributionPath;
	}

	public void setDistributionPath(String distributionPath) {
		this.distributionPath = distributionPath;
	}

	/**
	 * Get Duration time in HH:MM:SS style.
<<<<<<< HEAD
	 * 
	 * @return formatted duration string
	 */
	public String getDurationStr() {
		return DateUtil.ms2Time(this.duration);
=======
	 *
	 * @return formatted duration string
	 */
	public String getDurationStr() {
		return DateUtils.ms2Time(this.duration);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}


	/**
	 * Get Running time in HH:MM:SS style.
<<<<<<< HEAD
	 * 
=======
	 *
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return formatted runtime string
	 */
	public String getRuntimeStr() {
		long ms = (this.finishTime == null || this.startTime == null) ? 0 : this.finishTime.getTime()
<<<<<<< HEAD
						- this.startTime.getTime();
		return DateUtil.ms2Time(ms);
=======
				- this.startTime.getTime();
		return DateUtils.ms2Time(ms);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toStringExclude(this, "tags");
	}

	public String getProgressMessage() {
<<<<<<< HEAD
		return StringUtils.defaultIfEmpty(progressMessage, "");
=======
		return progressMessage;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	public void setProgressMessage(String progressMessage) {
		this.progressMessage = StringUtils.defaultIfEmpty(StringUtils.right(progressMessage, MAX_STRING_SIZE), "");
	}

	public Boolean getStopRequest() {
		return stopRequest;
	}

	public void setStopRequest(Boolean stopRequest) {
		this.stopRequest = stopRequest;
	}

	public String getLastProgressMessage() {
<<<<<<< HEAD
		return StringUtils.defaultIfEmpty(lastProgressMessage, "");
=======
		return lastProgressMessage;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Clear the last progress message.
	 */
	public void clearLastProgressMessage() {
		this.lastProgressMessage = "";
	}

	/**
	 * Set the last progress message.
<<<<<<< HEAD
	 * 
	 * @param lastProgressMessage
	 *            message
=======
	 *
	 * @param lastProgressMessage message
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 */
	public void setLastProgressMessage(String lastProgressMessage) {
		if (StringUtils.isEmpty(lastProgressMessage)) {
			return;
		}
		if (!StringUtils.equals(this.lastProgressMessage, lastProgressMessage)) {
			setProgressMessage(getProgressMessage() + this.lastProgressMessage + "\n");
		}
		this.lastProgressMessage = lastProgressMessage;
	}

	public String getTestComment() {
		return testComment;
	}

	public void setTestComment(String testComment) {
		this.testComment = StringUtils.trimToEmpty(StringUtils.right(testComment, MAX_STRING_SIZE));
	}

	public Long getScriptRevision() {
		return scriptRevision;
	}

<<<<<<< HEAD
	
=======

>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	public void setScriptRevision(Long scriptRevision) {
		this.scriptRevision = scriptRevision;
	}

	public String getDateString() {
		return dateString;
	}

	public void setDateString(String dateString) {
		this.dateString = dateString;
	}

	/**
	 * Clear all messages.
	 */
	public void clearMessages() {
		clearLastProgressMessage();
		setProgressMessage("");
	}

	public Boolean getUseRampUp() {
		return useRampUp;
	}


<<<<<<< HEAD
	@Cloneable
=======
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	public void setUseRampUp(Boolean useRampUp) {
		this.useRampUp = useRampUp;
	}

	public Boolean getSendMail() {
		return sendMail;
	}


<<<<<<< HEAD
	@Cloneable
=======
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	public void setSendMail(Boolean sendMail) {
		this.sendMail = sendMail;
	}

	public String getTagString() {
		return tagString;
	}


<<<<<<< HEAD
	@Cloneable
	@ForceMergable
=======
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	public void setTagString(String tagString) {
		this.tagString = tagString;
	}

	public SortedSet<Tag> getTags() {
		return tags;
	}

	public void setTags(SortedSet<Tag> tags) {
		this.tags = tags;
	}

	public String getRegion() {
		return region;
	}


<<<<<<< HEAD
	@Cloneable
=======
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	public void setRegion(String region) {
		this.region = region;
	}

	public Boolean getSafeDistribution() {
		return safeDistribution == null ? false : safeDistribution;
	}


<<<<<<< HEAD
	@Cloneable
=======
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	public void setSafeDistribution(Boolean safeDistribution) {
		this.safeDistribution = safeDistribution;
	}

	public String getRunningSample() {
		return runningSample;
	}


<<<<<<< HEAD
	@ForceMergable
=======
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	public void setRunningSample(String runningSample) {
		this.runningSample = runningSample;
	}

<<<<<<< HEAD
	public String getAgentStatus() {
		return agentStatus;
	}

	@ForceMergable
	public void setAgentStatus(String agentStatus) {
		this.agentStatus = agentStatus;
	}

	public String getMonitorStatus() {
		return monitorStatus;
	}

	@ForceMergable
	public void setMonitorStatus(String monitorStatus) {
		this.monitorStatus = monitorStatus;
=======
	public String getAgentState() {
		return agentState;
	}

	public void setAgentState(String agentStatus) {
		this.agentState = agentStatus;
	}

	public String getMonitorState() {
		return monitorState;
	}

	public void setMonitorState(String monitorStatus) {
		this.monitorState = monitorStatus;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	public Integer getSamplingInterval() {
		return samplingInterval;
	}


<<<<<<< HEAD
	@Cloneable
=======
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	public void setSamplingInterval(Integer samplingInterval) {
		this.samplingInterval = samplingInterval;
	}

	public String getParam() {
		return param;
	}

<<<<<<< HEAD
	@Cloneable
	public void setParam(String param) {
		this.param = param;
	}
	

=======
	public void setParam(String param) {
		this.param = param;
	}

	public void prepare(boolean isClone) {
		if (isClone) {
			this.setId(null);
			this.setTestComment("");
		}
		this.useRampUp = getSafe(this.useRampUp);
		this.safeDistribution = getSafe(this.safeDistribution);
	}
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
}
