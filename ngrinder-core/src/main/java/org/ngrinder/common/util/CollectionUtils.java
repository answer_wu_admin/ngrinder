package org.ngrinder.common.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Collection utilities for object creation.
<<<<<<< HEAD
 * 
=======
 *
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
 * @author JunHo Yoon
 * @since 3.1
 */
public abstract class CollectionUtils {
	/**
	 * Create new {@link HashMap}.
<<<<<<< HEAD
	 * 
	 * @param <K>
	 *            key
	 * @param <V>
	 *            value
=======
	 *
	 * @param <K> key
	 * @param <V> value
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return {@link HashMap}
	 */
	public static <K, V> Map<K, V> newHashMap() {
		return new HashMap<K, V>();
	}

	/**
	 * Create new {@link LinkedHashMap}.
<<<<<<< HEAD
	 * 
	 * @param <K>
	 *            key
	 * @param <V>
	 *            value
=======
	 *
	 * @param <K> key
	 * @param <V> value
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return {@link LinkedHashMap}
	 */
	public static <K, V> Map<K, V> newLinkedHashMap() {
		return new LinkedHashMap<K, V>();
	}

	/**
	 * Create new {@link HashSet}.
<<<<<<< HEAD
	 * 
	 * @param <K>
	 *            key
=======
	 *
	 * @param <K> key
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return {@link HashSet}
	 */
	public static <K> Set<K> newHashSet() {
		return new HashSet<K>();
	}

	/**
	 * Create new {@link LinkedHashSet}.
<<<<<<< HEAD
	 * 
	 * @param <K>
	 *            key
=======
	 *
	 * @param <K> key
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return {@link LinkedHashSet}
	 */
	public static <K> Set<K> newLinkedHashSet() {
		return new LinkedHashSet<K>();
	}

	/**
	 * Create new {@link ArrayList}.
<<<<<<< HEAD
	 * 
	 * @param <L>
	 *            type
=======
	 *
	 * @param <L>  type
	 * @param size size
	 * @return {@link ArrayList}
	 */
	public static <L> List<L> newArrayList(int size) {
		return new ArrayList<L>(size);
	}

	/**
	 * Create new {@link ArrayList}.
	 *
	 * @param <L> type
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return {@link ArrayList}
	 */
	public static <L> List<L> newArrayList() {
		return new ArrayList<L>();
	}

	/**
	 * Build Map with 1 pair.
<<<<<<< HEAD
	 * 
	 * @param <K>
	 *            key type
	 * @param <V>
	 *            value type
	 * @param key1
	 *            key
	 * @param value1
	 *            value
=======
	 *
	 * @param <K>    key type
	 * @param <V>    value type
	 * @param key1   key
	 * @param value1 value
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return created map
	 */
	public static <K, V> Map<K, V> buildMap(K key1, V value1) {
		Map<K, V> map = new HashMap<K, V>(1);
		map.put(key1, value1);
		return map;
	}

	/**
	 * Build Map with 2 pairs.
<<<<<<< HEAD
	 * 
	 * @param <K>
	 *            key type
	 * @param <V>
	 *            value type
	 * @param key1
	 *            key
	 * @param value1
	 *            value
	 * @param key2
	 *            key
	 * @param value2
	 *            value
=======
	 *
	 * @param <K>    key type
	 * @param <V>    value type
	 * @param key1   key
	 * @param value1 value
	 * @param key2   key
	 * @param value2 value
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return create map
	 */
	public static <K, V> Map<K, V> buildMap(K key1, V value1, K key2, V value2) {
		Map<K, V> map = new HashMap<K, V>(2);
		map.put(key1, value1);
		map.put(key2, value2);
		return map;
	}

	/**
	 * Build Map with 3 pairs.
<<<<<<< HEAD
	 * 
	 * @param <K>
	 *            key type
	 * @param <V>
	 *            value type
	 * 
	 * @param key1
	 *            key
	 * @param value1
	 *            value
	 * @param key2
	 *            key
	 * @param value2
	 *            value
	 * @param key3
	 *            key
	 * @param value3
	 *            value
=======
	 *
	 * @param <K>    key type
	 * @param <V>    value type
	 * @param key1   key
	 * @param value1 value
	 * @param key2   key
	 * @param value2 value
	 * @param key3   key
	 * @param value3 value
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return create map
	 */
	public static <K, V> Map<K, V> buildMap(K key1, V value1, K key2, V value2, K key3, V value3) {
		Map<K, V> map = new HashMap<K, V>(3);
		map.put(key1, value1);
		map.put(key2, value2);
		map.put(key3, value3);
		return map;
	}

	/**
	 * Select the given number of elements from the given set.
<<<<<<< HEAD
	 * 
	 * @param <T>
	 *            encapsulated type
	 * @param set
	 *            set
	 * @param count
	 *            number of elements to retrieve
=======
	 *
	 * @param <T>   encapsulated type
	 * @param set   set
	 * @param count number of elements to retrieve
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return set
	 */
	public static <T> Set<T> selectSome(Set<T> set, int count) {
		Set<T> newSet = new HashSet<T>();
		int i = 0;
		for (T each : set) {
			if (++i > count) {
				break;
			}
			newSet.add(each);
		}
		return newSet;
	}

	/**
	 * Create new {@link HashMap}.
<<<<<<< HEAD
	 * 
	 * @param size
	 *            size of {@link HashMap}
	 * @param <K>
	 *            keyType
	 * @param <V>
	 *            valueType
=======
	 *
	 * @param size size of {@link HashMap}
	 * @param <K>  keyType
	 * @param <V>  valueType
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return created {@link HashMap} instance
	 */
	public static <K, V> HashMap<K, V> newHashMap(int size) {
		return new HashMap<K, V>(size);
	}

	/**
	 * Create new {@link HashMap} having same size of given base collection.
<<<<<<< HEAD
	 * 
	 * @param base
	 *            collection which size will be referred
	 * @param <K>
	 *            keyType
	 * @param <V>
	 *            valueType
=======
	 *
	 * @param base collection which size will be referred
	 * @param <K>  keyType
	 * @param <V>  valueType
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return created {@link HashMap} instance
	 */
	public static <K, V> HashMap<K, V> newHashMap(Collection<?> base) {
		return new HashMap<K, V>(base.size());
	}

	/**
	 * Get the value from the map for the given key. It the value does not exist, return the given
	 * defaultValue.
<<<<<<< HEAD
	 * 
	 * @param <K>
	 *            key type
	 * @param <V>
	 *            value type
	 * 
	 * @param map
	 *            map
	 * @param key
	 *            key
	 * @param defaultValue
	 *            default value if the value is null.
=======
	 *
	 * @param <K>          key type
	 * @param <V>          value type
	 * @param map          map
	 * @param key          key
	 * @param defaultValue default value if the value is null.
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return value
	 */
	public static <K, V> V getValue(Map<K, V> map, K key, V defaultValue) {
		V v = map.get(key);
		if (v == null) {
			return defaultValue;
		}
		return v;
	}
<<<<<<< HEAD
=======

>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
}
