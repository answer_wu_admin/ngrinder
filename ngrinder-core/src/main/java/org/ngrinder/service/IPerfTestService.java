/* 
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */
package org.ngrinder.service;

<<<<<<< HEAD
import java.io.File;
import java.util.Date;
import java.util.List;

=======
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
import org.ngrinder.model.PerfTest;
import org.ngrinder.model.Status;
import org.ngrinder.model.User;

<<<<<<< HEAD
=======
import java.io.File;
import java.util.Date;
import java.util.List;

>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
/**
 * {@link PerfTest} service interface. This is visible from plugin.
 * 
 * @author JunHo Yoon
 * @since 3.0
 */
public interface IPerfTestService {

	/**
	 * get test detail.
	 * 
<<<<<<< HEAD
	 * @param user
	 *            current operation user.
	 * @param id
	 *            test id
	 * @return perftestDetail perftest detail
	 */
	public abstract PerfTest getPerfTest(User user, Long id);
=======
	 * @param user	current operation user.
	 * @param id	test id
	 * @return perftest	{@link PerfTest}
	 */
	public abstract PerfTest getOne(User user, Long id);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad

	/**
	 * Get {@link PerfTest} list created within the given time frame.
	 * 
<<<<<<< HEAD
	 * @param start
	 *            start time.
	 * @param end
	 *            end time.
	 * 
	 * @return found {@link PerfTest} list
	 */
	public abstract List<PerfTest> getPerfTest(Date start, Date end);
=======
	 * @param start	start time.
	 * @param end	end time.
	 * @return found {@link PerfTest} list
	 */
	public abstract List<PerfTest> getAll(Date start, Date end);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad

	/**
	 * Get {@link PerfTest} list created within the given time frame and region name.
	 * 
<<<<<<< HEAD
	 * @param start
	 *            start time.
	 * @param end
	 *            end time.
	 * @param region
	 *            region
	 * 
	 * @return found {@link PerfTest} list
	 */
	public abstract List<PerfTest> getPerfTest(Date start, Date end, String region);
=======
	 * @param start		start time.
	 * @param end		end time.
	 * @param region	region
	 * @return found {@link PerfTest} list
	 */
	public abstract List<PerfTest> getAll(Date start, Date end, String region);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad

	/**
	 * Get {@link PerfTest} list of some IDs.
	 * 
<<<<<<< HEAD
	 * @param user
	 *            current operation user
	 * @param ids
	 *            test IDs, which is in format: "1,3,6,11"
	 * @return perftestList test list of those IDs
	 */
	public abstract List<PerfTest> getPerfTest(User user, Long[] ids);
=======
	 * @param user	current operation user
	 * @param ids	test IDs, which is in format: "1,3,6,11"
	 * @return perftest list test list of those IDs
	 */
	public abstract List<PerfTest> getAll(User user, Long[] ids);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad

	/**
	 * Get PerfTest count which have given status.
	 * 
<<<<<<< HEAD
	 * @param user
	 *            user who created test. null to retrieve all
	 * @param statuses
	 *            status set
	 * @return the count
	 */
	public abstract long getPerfTestCount(User user, Status... statuses);
=======
	 * @param user		user who created test. null to retrieve all
	 * @param statuses	status set
	 * @return the count
	 */
	public abstract long count(User user, Status[] statuses);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad

	/**
	 * Get {@link PerfTest} list which have give state.
	 * 
<<<<<<< HEAD
	 * @param user
	 *            user who created {@link PerfTest}. if null, retrieve all test
	 * @param statuses
	 *            set of {@link Status}
	 * @return found {@link PerfTest} list.
	 */
	public abstract List<PerfTest> getPerfTest(User user, Status... statuses);
=======
	 * @param user		user who created {@link PerfTest}. if null, retrieve all test
	 * @param statuses	set of {@link Status}
	 * @return found {@link PerfTest} list.
	 */
	public abstract List<PerfTest> getAll(User user, Status[] statuses);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad

	/**
	 * Save {@link PerfTest}. This function includes logic the updating script revision when it's
	 * READY status.
	 * 
<<<<<<< HEAD
	 * @param user
	 *            user
	 * @param perfTest
	 *            {@link PerfTest} instance to be saved.
	 * @return Saved {@link PerfTest}
	 */
	public abstract PerfTest savePerfTest(User user, PerfTest perfTest);

	/**
	 * Save {@link PerfTest}.
	 * 
	 * @param perfTest
	 *            {@link PerfTest} instance to be saved.
	 * @return Saved {@link PerfTest}
	 */
	public abstract PerfTest savePerfTest(PerfTest perfTest);
=======
	 * @param user		user
	 * @param perfTest	{@link PerfTest} instance to be saved.
	 * @return Saved {@link PerfTest}
	 */
	public abstract PerfTest save(User user, PerfTest perfTest);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad

	/**
	 * Get PerfTest by testId.
	 * 
<<<<<<< HEAD
	 * @param testId
	 *            PerfTest id
	 * @return found {@link PerfTest}, null otherwise
	 */
	public abstract PerfTest getPerfTest(Long testId);
=======
	 * @param testId	PerfTest id
	 * @return found {@link PerfTest}, null otherwise
	 */
	public abstract PerfTest getOne(Long testId);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad

	/**
	 * Get PerfTest with tag infos by testId.
	 * 
<<<<<<< HEAD
	 * @param testId
	 *            PerfTest id
	 * @return found {@link PerfTest}, null otherwise
	 */
	public abstract PerfTest getPerfTestWithTag(Long testId);
=======
	 * @param testId	PerfTest id
	 * @return found {@link PerfTest}, null otherwise
	 */
	public abstract PerfTest getOneWithTag(Long testId);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad

	/**
	 * Get currently testing PerfTest.
	 * 
	 * @return found {@link PerfTest} list
	 */
<<<<<<< HEAD
	public abstract List<PerfTest> getTestingPerfTest();
=======
	public abstract List<PerfTest> getAllTesting();
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad

	/**
	 * Get PerfTest Directory in which the distributed file is stored.
	 * 
<<<<<<< HEAD
	 * @param perfTest
	 *            pefTest from which distribution dire.ctory calculated
	 * @return path on in files are saved.
	 */
	public abstract File getPerfTestDistributionPath(PerfTest perfTest);
=======
	 * @param perfTest	pefTest from which distribution directory calculated
	 * @return path on in files are saved.
	 */
	public abstract File getDistributionPath(PerfTest perfTest);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad

	/**
	 * Get perf test base directory.
	 * 
<<<<<<< HEAD
	 * @param perfTest
	 *            perfTest
=======
	 * @param perfTest	perfTest
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return directory prefTest base path
	 */
	public abstract File getPerfTestDirectory(PerfTest perfTest);

	/**
	 * Get all perf test list.
	 * 
	 * Note : This is only for test
	 * 
	 * @return all {@link PerfTest} list
	 * 
	 */
	public abstract List<PerfTest> getAllPerfTest();

	/**
	 * Mark Stop on {@link PerfTest}.
	 * 
<<<<<<< HEAD
	 * @param user
	 *            user
	 * @param id
	 *            perftest id
	 */
	public abstract void stopPerfTest(User user, Long id);
=======
	 * @param user	user
	 * @param id	perftest id
	 */
	public abstract void stop(User user, Long id);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad

	/**
	 * Return stop requested test.
	 * 
	 * @return stop requested perf test
	 */
<<<<<<< HEAD
	public abstract List<PerfTest> getStopRequestedPerfTest();
=======
	public abstract List<PerfTest> getAllStopRequested();
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad

	/**
	 * Add comment on {@link PerfTest}.
	 * 
<<<<<<< HEAD
	 * @param user
	 *            current operated user
	 * @param testId
	 *            perftest id
	 * @param testComment
	 *            comment
	 * @param tagString
	 *            tagString
=======
	 * @param user			current operated user
	 * @param testId		perftest id
	 * @param testComment	comment
	 * @param tagString		tagString
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 */
	public abstract void addCommentOn(User user, Long testId, String testComment, String tagString);

	/**
	 * Save performance test with given status.
	 * 
	 * This method is only used for changing {@link Status}
	 * 
<<<<<<< HEAD
	 * @param perfTest
	 *            {@link PerfTest} instance which will be saved.
	 * @param status
	 *            Status to be assigned
	 * @param message
	 *            progress message
=======
	 * @param perfTest	{@link PerfTest} instance which will be saved.
	 * @param status	Status to be assigned
	 * @param message	progress message
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return saved {@link PerfTest}
	 */
	public abstract PerfTest markStatusAndProgress(PerfTest perfTest, Status status, String message);

	/**
	 * Get performance test statistic path.
	 * 
<<<<<<< HEAD
	 * @param perfTest
	 *            perftest
	 * @return statistic path
	 */
	public abstract File getPerfTestStatisticPath(PerfTest perfTest);
=======
	 * @param perfTest	perftest
	 * @return statistic path
	 */
	@SuppressWarnings("UnusedDeclaration")
	public abstract File getStatisticPath(PerfTest perfTest);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad

}