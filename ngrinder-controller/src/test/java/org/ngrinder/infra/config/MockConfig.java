/* 
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */
package org.ngrinder.infra.config;

<<<<<<< HEAD
import java.util.Properties;

import org.ngrinder.common.util.PropertiesWrapper;
import org.ngrinder.infra.annotation.TestOnlyComponent;

@TestOnlyComponent
public class MockConfig extends Config {
	private PropertiesWrapper wrapper = new PropertiesWrapper(new Properties());
=======
import org.ngrinder.common.util.PropertiesWrapper;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.Properties;

@Profile("unit-test")
@Component
public class MockConfig extends Config {
	private PropertiesWrapper wrapper = new PropertiesWrapper(new Properties(), controllerPropertiesKeyMapper);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad

	public boolean cluster;
	public boolean doRealOnRegion = false;

<<<<<<< HEAD
	public void setSystemProperties(PropertiesWrapper wrapper) {
=======
	public void setControllerProperties(PropertiesWrapper wrapper) {
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		this.wrapper = wrapper;
	}

	@Override
<<<<<<< HEAD
	public PropertiesWrapper getSystemProperties() {
=======
	public PropertiesWrapper getControllerProperties() {
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		return wrapper;
	}

	@Override
<<<<<<< HEAD
	public void loadSystemProperties() {
		super.loadSystemProperties();
		setSystemProperties(super.getSystemProperties());
	}

	@Override
	public boolean isCluster() {
=======
	public void loadProperties() {
		super.loadProperties();
		setControllerProperties(super.getControllerProperties());
	}

	@Override
	public boolean isClustered() {
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		// TODO Auto-generated method stub
		return cluster;
	}

	@Override
	public String getRegion() {
<<<<<<< HEAD
		return isCluster() ? (doRealOnRegion == true ? super.getRegion() : "TestRegion") : NONE_REGION;
=======
		return isClustered() ? (doRealOnRegion == true ? super.getRegion() : "TestRegion") : NONE_REGION;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

}
