/* 
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */
package org.ngrinder.common.model;

<<<<<<< HEAD
import static org.ngrinder.common.util.ExceptionUtils.processException;
import static org.ngrinder.common.util.Preconditions.checkNotNull;
=======
import org.apache.commons.io.FileUtils;
import org.ngrinder.common.constants.GrinderConstants;
import org.ngrinder.common.exception.ConfigurationException;
import org.ngrinder.common.util.EncodingUtils;
import org.ngrinder.common.util.NoOp;
import org.ngrinder.model.PerfTest;
import org.ngrinder.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.Properties;

<<<<<<< HEAD
import org.apache.commons.io.FileUtils;
import org.ngrinder.common.constant.NGrinderConstants;
import org.ngrinder.common.exception.ConfigurationException;
import org.ngrinder.common.util.EncodingUtil;
import org.ngrinder.common.util.NoOp;
import org.ngrinder.model.PerfTest;
import org.ngrinder.model.User;
=======
import static org.ngrinder.common.util.ExceptionUtils.processException;
import static org.ngrinder.common.util.Preconditions.checkNotNull;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad

/**
 * Home class which enables the easy resource access in ${NGRINDER_HOME}
 * directory.
<<<<<<< HEAD
 * 
 * @author JunHo Yoon
 * @since 3.0
 */
public class Home implements NGrinderConstants {

	private final File directory;

	/**
	 * Constructor.
	 * 
	 * @param directory
	 *            home directory
=======
 *
 * @author JunHo Yoon
 * @since 3.0
 */
public class Home {

	// HOME_PATH
	private static final String PATH_PLUGIN = "plugins";
	private static final String PATH_SCRIPT = "script";
	private static final String PATH_USER_REPO = "repos";
	private static final String PATH_PERF_TEST = "perftest";
	private static final String PATH_DOWNLOAD = "download";
	private static final String PATH_GLOBAL_LOG = "logs";
	private static final String PATH_LOG = "logs";
	private static final String PATH_REPORT = "report";
	private static final String PATH_DIST = "dist";
	private static final String PATH_STAT = "stat";
	private final static Logger LOGGER = LoggerFactory.getLogger(Home.class);
	private final File directory;
	public static final String REPORT_CSV = "output.csv";

	/**
	 * Constructor.
	 *
	 * @param directory home directory
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 */
	public Home(File directory) {
		this(directory, true);
	}

	/**
	 * Constructor.
<<<<<<< HEAD
	 * 
	 * @param directory
	 *            home directory ${NGRINDER_HOME}
	 * @param create
	 *            create the directory if not exists
=======
	 *
	 * @param directory home directory ${NGRINDER_HOME}
	 * @param create    create the directory if not exists
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 */
	public Home(File directory, boolean create) {
		checkNotNull(directory, "directory should not be null");
		if (create) {
<<<<<<< HEAD
			directory.mkdir();
=======
			if (directory.mkdir()) {
				LOGGER.info("{} is created.", directory.getPath());
			}
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		}
		if (directory.exists() && !directory.canWrite()) {
			throw new ConfigurationException(String.format(" ngrinder home directory %s is not writable.", directory),
					null);
		}
		this.directory = directory;
	}

<<<<<<< HEAD
	/**
	 * Get the home directory.
	 * 
=======
	public void init() {
		makeSubPath(PATH_PLUGIN);
		makeSubPath(PATH_PERF_TEST);
		makeSubPath(PATH_DOWNLOAD);
	}

	/**
	 * Get the home directory.
	 *
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return home directory
	 */
	public File getDirectory() {
		return directory;
	}

	/**
	 * Copy the given file from given location.
<<<<<<< HEAD
	 * 
	 * @param from
	 *            file location
	 * @param overwrite
	 *            overwrite
	 */
	public void copyFrom(File from, boolean overwrite) {
		// Copy missing files
		try {
			for (File file : from.listFiles()) {
=======
	 *
	 * @param from file location
	 */
	public void copyFrom(File from) {
		// Copy missing files
		try {
			for (File file : checkNotNull(from.listFiles())) {
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
				if (!(new File(directory, file.getName()).exists())) {
					FileUtils.copyFileToDirectory(file, directory);
				} else {
					File orgConf = new File(directory, "org_conf");
<<<<<<< HEAD
					orgConf.mkdirs();
=======
					if (orgConf.mkdirs()) {
						LOGGER.info("{}", orgConf.getPath());
					}
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
					FileUtils.copyFile(file, new File(orgConf, file.getName()));
				}
			}
		} catch (IOException e) {
			throw processException("Fail to copy files from " + from.getAbsolutePath(), e);
		}
	}

	/**
	 * Make a sub directory on the home directory.
<<<<<<< HEAD
	 * 
	 * @param subPathName
	 *            sub-path name
	 */
	public void makeSubPath(String subPathName) {
		File subFile = new File(directory, subPathName);
		if (!subFile.exists()) {
			subFile.mkdir();
		}
=======
	 *
	 * @param subPathName sub-path name
	 */
	public void makeSubPath(String subPathName) {
		mkDir(new File(directory, subPathName));
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Get the {@link Properties} from the the given configuration file.
<<<<<<< HEAD
	 * 
	 * @param confFileName
	 *            configuration file name
=======
	 *
	 * @param confFileName configuration file name
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return loaded {@link Properties}
	 */
	public Properties getProperties(String confFileName) {
		try {
			File configFile = getSubFile(confFileName);
			if (configFile.exists()) {
				byte[] propByte = FileUtils.readFileToByteArray(configFile);
<<<<<<< HEAD
				String propString = EncodingUtil.getAutoDecodedString(propByte, "UTF-8");
=======
				String propString = EncodingUtils.getAutoDecodedString(propByte, "UTF-8");
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
				Properties prop = new Properties();
				prop.load(new StringReader(propString));
				return prop;
			} else {
				// default empty properties.
				return new Properties();
			}

		} catch (IOException e) {
			throw processException("Fail to load property file " + confFileName, e);
		}
	}

	/**
	 * Get the sub {@link File} instance under the home directory.
<<<<<<< HEAD
	 * 
	 * @param subPathName
	 *            subpath name
=======
	 *
	 * @param subPathName subpath name
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return {@link File}
	 */
	public File getSubFile(String subPathName) {
		return new File(directory, subPathName);
	}

	/**
<<<<<<< HEAD
	 * Get the script base directory.
	 * 
	 * @return script base directory.
	 */
	public File getScriptDirectory() {
		return getSubFile(SCRIPT_PATH);
	}

	/**
	 * Get the script directory for the given user.
	 * 
	 * @param user
	 *            user
	 * @return script directory for the given user.
	 */
	public File getScriptDirectory(User user) {
		return new File(getSubFile(SCRIPT_PATH), user.getUserId());
=======
	 * Get the plugin cache directory.
	 *
	 * @return plugin cache directory.
	 */
	public File getPluginsCacheDirectory() {
		return getSubFile(PATH_PLUGIN + "_cache");
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Get the plugin directory.
<<<<<<< HEAD
	 * 
	 * @return plugin directory.
	 */
	public File getPluginsDirectory() {
		return getSubFile(PLUGIN_PATH);
=======
	 *
	 * @return plugin directory.
	 */
	public File getPluginsDirectory() {
		return getSubFile(PATH_PLUGIN);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Get the repo base directory.
<<<<<<< HEAD
	 * 
	 * @return repo base directory.
	 */
	public File getRepoDirectoryRoot() {
		return getSubFile(USER_REPO_PATH);
=======
	 *
	 * @return repo base directory.
	 */
	public File getRepoDirectoryRoot() {
		return getSubFile(PATH_USER_REPO);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Get the user repo directory for the given user.
<<<<<<< HEAD
	 * 
	 * @param user
	 *            user
=======
	 *
	 * @param user user
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return user repo directory.
	 */
	public File getUserRepoDirectory(User user) {
		return getUserRepoDirectory(user.getUserId());
	}

	/**
	 * Get the sub directory of the base user repo directory.
<<<<<<< HEAD
	 * 
	 * @param subPath
	 *            subPath
=======
	 *
	 * @param subPath subPath
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return base repo sub directory.
	 */
	public File getUserRepoDirectory(String subPath) {
		return new File(getRepoDirectoryRoot(), subPath);
	}

	/**
	 * Get the base perftest directory.
<<<<<<< HEAD
	 * 
	 * @return base perftest directory.
	 */
	public File getPerfTestDirectory() {
		return getSubFile(PERF_TEST_PATH);
=======
	 *
	 * @return base perftest directory.
	 */
	public File getPerfTestDirectory() {
		return getSubFile(PATH_PERF_TEST);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Get the sub directory for the given perftest.
<<<<<<< HEAD
	 * 
	 * @param perfTest
	 *            perfTest
	 * @param subPath
	 *            subPath
	 * @return {@link PerfTest} sub directory.
	 */
	private File getPerfTestSubDirectory(PerfTest perfTest, String subPath) {
		File file = new File(getPerfTestDirectory(perfTest), subPath);
		file.mkdirs();
=======
	 *
	 * @param perfTest perfTest
	 * @param subPath  subPath
	 * @return {@link PerfTest} sub directory.
	 */
	private File getPerfTestSubDirectory(PerfTest perfTest, String subPath) {
		return mkDir(new File(getPerfTestDirectory(perfTest), subPath));
	}

	private File mkDir(File file) {
		if (file.mkdirs()) {
			LOGGER.info("{} is created.", file.getPath());
		}
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		return file;
	}

	/**
	 * Get the sub directory of the given perftest's base directory.
<<<<<<< HEAD
	 * 
	 * @param id
	 *            perfTest id
	 * @param subPath
	 *            subPath
=======
	 *
	 * @param id      perfTest id
	 * @param subPath subPath
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return {@link PerfTest} sub directory.
	 */
	public File getPerfTestSubDirectory(String id, String subPath) {
		File file = new File(getPerfTestDirectory(id), subPath);
<<<<<<< HEAD
		file.mkdirs();
		return file;
=======
		return mkDir(file);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Get the perftest base directory for the given perftest id.
<<<<<<< HEAD
	 * 
	 * @param id
	 *            perftest id
=======
	 *
	 * @param id perftest id
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return {@link PerfTest} sub directory.
	 */
	public File getPerfTestDirectory(String id) {
		File file = new File(getPerfTestDirectory(), id);
		// For backward compatibility
		if (!file.exists()) {
			file = getDistributedFolderName(id);
		}
<<<<<<< HEAD
		file.mkdirs();
		return file;
	}

	File getDistributedFolderName(String id) {
		File file;
=======
		return mkDir(file);
	}

	File getDistributedFolderName(String id) {
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		int numericId = 0;
		try {
			numericId = (Integer.parseInt(id) / 1000) * 1000;
		} catch (NumberFormatException e) {
			NoOp.noOp();
		}
		String folderName = String.format("%d_%d%s%s", numericId, numericId + 999, File.separator, id);
<<<<<<< HEAD
		file = new File(getPerfTestDirectory(), folderName);
		return file;
=======
		return new File(getPerfTestDirectory(), folderName);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Get the root directory for given {@link PerfTest} id.
<<<<<<< HEAD
	 * 
	 * @param perfTest
	 *            perftest
=======
	 *
	 * @param perfTest perftest
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return {@link PerfTest} log directory
	 */
	public File getPerfTestDirectory(PerfTest perfTest) {
		return getPerfTestDirectory(String.valueOf(perfTest.getId()));
	}

	/**
	 * Get the log directory for given {@link PerfTest} id.
<<<<<<< HEAD
	 * 
	 * @param id
	 *            perftest id
=======
	 *
	 * @param id perftest id
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return {@link PerfTest} log directory
	 */
	public File getPerfTestLogDirectory(String id) {
		return getPerfTestSubDirectory(id, PATH_LOG);
	}

	/**
	 * Get the log directory for given {@link PerfTest}.
<<<<<<< HEAD
	 * 
	 * @param perfTest
	 *            perftest
=======
	 *
	 * @param perfTest perftest
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return {@link PerfTest} log directory
	 */
	public File getPerfTestLogDirectory(PerfTest perfTest) {
		return getPerfTestSubDirectory(perfTest, PATH_LOG);
	}

	/**
	 * Get the distribution directory for given {@link PerfTest}.
<<<<<<< HEAD
	 * 
	 * @param perfTest
	 *            perftest
=======
	 *
	 * @param perfTest perftest
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return {@link PerfTest} distribution directory
	 */
	public File getPerfTestDistDirectory(PerfTest perfTest) {
		return getPerfTestSubDirectory(perfTest, PATH_DIST);
	}

	/**
	 * Get the statistics directory for given {@link PerfTest}.
<<<<<<< HEAD
	 * 
	 * @param perfTest
	 *            perftest
=======
	 *
	 * @param perfTest perftest
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return {@link PerfTest} statistics directory
	 */
	public File getPerfTestStatisticPath(PerfTest perfTest) {
		return getPerfTestSubDirectory(perfTest, PATH_STAT);
	}

	/**
	 * Get the report directory for given {@link PerfTest} id.
<<<<<<< HEAD
	 * 
	 * @param id
	 *            perftest id
=======
	 *
	 * @param id perftest id
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return {@link PerfTest} report directory
	 */
	public File getPerfTestReportDirectory(String id) {
		return getPerfTestSubDirectory(id, PATH_REPORT);
	}

	/**
	 * Get the report directory for given {@link PerfTest}.
<<<<<<< HEAD
	 * 
	 * @param perfTest
	 *            perftest
=======
	 *
	 * @param perfTest perftest
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return {@link PerfTest} report directory
	 */
	public File getPerfTestReportDirectory(PerfTest perfTest) {
		return getPerfTestSubDirectory(perfTest, PATH_REPORT);
	}

	/**
<<<<<<< HEAD
	 * Get the default grinder properties file.
	 * 
	 * @return grinder properties file
	 */
	public File getDefaultGrinderProperties() {
		return getSubFile(DEFAULT_GRINDER_PROPERTIES_PATH);
=======
	 * Get the csv file for given {@link PerfTest}.
	 *
	 * @param perfTest perftest
	 * @return {@link PerfTest} csv file
	 */
	public File getPerfTestCsvFile(PerfTest perfTest) {
		return new File(getPerfTestReportDirectory(perfTest), REPORT_CSV);
	}

	/**
	 * Get the default grinder properties file.
	 *
	 * @return grinder properties file
	 */
	public File getDefaultGrinderProperties() {
		return getSubFile(GrinderConstants.DEFAULT_GRINDER_PROPERTIES);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Get the download directory.
<<<<<<< HEAD
	 * 
	 * @return download directory
	 */
	public File getDownloadDirectory() {
		return getSubFile(DOWNLOAD_PATH);
	}

	/**
	 * Get the controller share directory.
	 * 
	 * @return controller share directory
	 * @deprecated
	 */
	public File getControllerShareDirectory() {
		File subFile = getSubFile(SHARE_PATH);
		File controller = new File(subFile, CONTROLLER_PATH);
		controller.mkdirs();
		return controller;
	}

	/**
	 * Get global log file.
	 * 
	 * @return log file
	 */
	public File getGlobalLogFile() {
		File subFile = getSubFile(GLOBAL_LOG_PATH);
		return subFile;
=======
	 *
	 * @return download directory
	 */
	public File getDownloadDirectory() {
		return getSubFile(PATH_DOWNLOAD);
	}


	/**
	 * Get global log file.
	 *
	 * @return log file
	 */
	public File getGlobalLogFile() {
		return getSubFile(PATH_GLOBAL_LOG);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Check if this home exists.
<<<<<<< HEAD
	 * 
=======
	 *
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return true if exists.
	 */
	public boolean exists() {
		return directory.exists();
	}

	/**
	 * Get the user defined messages directory.
<<<<<<< HEAD
	 * 
=======
	 *
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return the user defined messages directory
	 */
	public File getMessagesDirectory() {
		return getSubFile("messages");
	}
<<<<<<< HEAD
}
=======

	/**
	 * Get the script directory for the given user.
	 *
	 * @param user user
	 * @return script directory for the given user.
	 */
	public File getScriptDirectory(User user) {
		return new File(getSubFile(PATH_SCRIPT), user.getUserId());
	}
}
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
