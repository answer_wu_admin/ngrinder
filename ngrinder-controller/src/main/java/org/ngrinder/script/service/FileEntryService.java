/* 
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */
package org.ngrinder.script.service;

<<<<<<< HEAD
import static org.ngrinder.common.util.CollectionUtils.buildMap;
import static org.ngrinder.common.util.CollectionUtils.newHashMap;
import static org.ngrinder.common.util.ExceptionUtils.processException;
import static org.ngrinder.common.util.Preconditions.checkNotEmpty;
import static org.ngrinder.common.util.Preconditions.checkNotNull;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.ngrinder.common.util.HttpContainerContext;
import org.ngrinder.common.util.PathUtil;
=======
import org.ngrinder.common.util.PathUtils;
import org.ngrinder.common.util.ThreadUtils;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
import org.ngrinder.common.util.UrlUtils;
import org.ngrinder.infra.config.Config;
import org.ngrinder.model.User;
import org.ngrinder.script.handler.ProjectHandler;
import org.ngrinder.script.handler.ScriptHandler;
import org.ngrinder.script.handler.ScriptHandlerFactory;
import org.ngrinder.script.model.FileEntry;
import org.ngrinder.script.model.FileType;
import org.ngrinder.script.repository.FileEntryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
<<<<<<< HEAD
=======
import org.springframework.cache.Cache;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.internal.io.fs.FSHook;
import org.tmatesoft.svn.core.internal.io.fs.FSHookEvent;
import org.tmatesoft.svn.core.internal.io.fs.FSHooks;
import org.tmatesoft.svn.core.wc.SVNClientManager;
import org.tmatesoft.svn.core.wc.SVNRevision;

<<<<<<< HEAD
/**
 * File entry service class.<br/>
 * 
 * This class is responsible for creating user svn repository whenever a user is
 * created and connect the user to the underlying svn.
 * 
=======
import javax.annotation.PostConstruct;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.Collections.unmodifiableList;
import static org.ngrinder.common.util.CollectionUtils.buildMap;
import static org.ngrinder.common.util.CollectionUtils.newHashMap;
import static org.ngrinder.common.util.ExceptionUtils.processException;
import static org.ngrinder.common.util.Preconditions.checkNotEmpty;
import static org.ngrinder.common.util.Preconditions.checkNotNull;

/**
 * File entry service class.
 *
 * This class is responsible for creating user svn repository whenever a user is
 * created and connect the user to the underlying svn.
 *
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
 * @author JunHo Yoon
 * @since 3.0
 */
@Service
public class FileEntryService {

	private static final Logger LOG = LoggerFactory.getLogger(FileEntryService.class);

	private SVNClientManager svnClientManager;

	@Autowired
	private Config config;

<<<<<<< HEAD
	@Autowired
	private HttpContainerContext httpContainerContext;
=======
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad

	@Autowired
	@Qualifier("cacheManager")
	private CacheManager cacheManager;

<<<<<<< HEAD
=======
	@SuppressWarnings("SpringJavaAutowiringInspection")
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	@Autowired
	private FileEntryRepository fileEntityRepository;

	@Autowired
	private ScriptHandlerFactory scriptHandlerFactory;

<<<<<<< HEAD
=======
	private Cache fileEntryCache;

>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	/**
	 * Initialize {@link FileEntryService}.
	 */
	@PostConstruct
	public void init() {
		// Add cache invalidation hook.
		FSHooks.registerHook(new FSHook() {
			@Override
			public void onHook(FSHookEvent event) throws SVNException {
				if (event.getType().equals(FSHooks.SVN_REPOS_HOOK_POST_COMMIT)) {
					String name = event.getReposRootDir().getName();
					invalidateCache(name);
				}
			}
		});
		svnClientManager = fileEntityRepository.getSVNClientManager();
<<<<<<< HEAD
	}

	/**
	 * invalidate the file_entry_seach_cache.
	 * 
	 * @param userId
	 *            userId.
	 */
	public void invalidateCache(String userId) {
		cacheManager.getCache("file_entry_search_cache").evict(userId);
=======
		fileEntryCache = cacheManager.getCache("file_entries");

	}

	/**
	 * invalidate the file_entry_search_cache.
	 *
	 * @param userId userId.
	 */
	public void invalidateCache(String userId) {
		fileEntryCache.evict(userId);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Create user svn repo.
<<<<<<< HEAD
	 * 
	 * This method is executed async way.
	 * 
	 * @param user
	 *            newly created user.
=======
	 *
	 * This method is executed async way.
	 *
	 * @param user newly created user.
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 */
	@Async
	public void prepare(User user) {
		File newUserDirectory = getUserRepoDirectory(user);
		try {
			if (!newUserDirectory.exists()) {
				createUserRepo(user, newUserDirectory);
			}
		} catch (SVNException e) {
			LOG.error("Error while prepare user {}'s repo", user.getUserName(), e);
		}
	}

	private SVNURL createUserRepo(User user, File newUserDirectory) throws SVNException {
		return svnClientManager.getAdminClient().doCreateRepository(newUserDirectory, user.getUserId(), true, true);
	}

	private File getUserRepoDirectory(User user) {
		return new File(config.getHome().getRepoDirectoryRoot(), checkNotNull(user.getUserId()));
	}

	/**
	 * Get all {@link FileEntry} for the given user. This method is subject to
	 * be cached because it takes time.
<<<<<<< HEAD
	 * 
	 * @param user
	 *            user
	 * @return cached {@link FileEntry} list
	 */
	@Cacheable(value = "file_entry_search_cache", key = "#user.userId")
	public List<FileEntry> getAllFileEntries(User user) {
		return fileEntityRepository.findAll(user);
=======
	 *
	 * @param user user
	 * @return cached {@link FileEntry} list
	 */
	@Cacheable(value = "file_entries", key = "#user.userId")
	public List<FileEntry> getAll(User user) {
		prepare(user);
		List<FileEntry> allFileEntries;
		try {
			allFileEntries = fileEntityRepository.findAll(user);
		} catch (Exception e) {
			// Try once more for the case of the underlying file system fault.
			ThreadUtils.sleep(3000);
			allFileEntries = fileEntityRepository.findAll(user);
		}
		return unmodifiableList(allFileEntries);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Get file entries from underlying svn for given path.
<<<<<<< HEAD
	 * 
	 * @param user
	 *            the user
	 * @param path
	 *            path in the repo
	 * @param revision
	 *            revision number. -1 if HEAD.
	 * @return file entry list
	 */
	public List<FileEntry> getFileEntries(User user, String path, Long revision) {
=======
	 *
	 * @param user     the user
	 * @param path     path in the repo
	 * @param revision revision number. -1 if HEAD.
	 * @return file entry list
	 */
	public List<FileEntry> getAll(User user, String path, Long revision) {
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		// If it's not created, make one.
		prepare(user);
		return fileEntityRepository.findAll(user, path, revision);
	}

	/**
<<<<<<< HEAD
	 * Get single file entity.
	 * 
	 * The return value has content byte.
	 * 
	 * @param user
	 *            the user
	 * @param path
	 *            path in the svn repo
	 * @param revision
	 *            file revision.
	 * @return single file entity
	 */
	public FileEntry getFileEntry(User user, String path, long revision) {
		return fileEntityRepository.findOne(user, path, SVNRevision.create(revision));
=======
	 * Get file entity for the given revision.
	 *
	 * @param user     the user
	 * @param path     path in the repo
	 * @param revision revision. if -1, HEAD
	 * @return file entity
	 */
	public FileEntry getOne(User user, String path, Long revision) {
		SVNRevision svnRev = (revision == null || revision == -1) ? SVNRevision.HEAD : SVNRevision.create(revision);
		return fileEntityRepository.findOne(user, path, svnRev);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Get single file entity.
<<<<<<< HEAD
	 * 
	 * The return value has content byte.
	 * 
	 * @param user
	 *            the user
	 * @param path
	 *            path in the svn repo
	 * @return single file entity
	 */
	public FileEntry getFileEntry(User user, String path) {
		return fileEntityRepository.findOne(user, path, SVNRevision.HEAD);
=======
	 *
	 * The return value has content byte.
	 *
	 * @param user the user
	 * @param path path in the svn repo
	 * @return single file entity
	 */
	public FileEntry getOne(User user, String path) {
		return getOne(user, path, -1L);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Check file existence.
<<<<<<< HEAD
	 * 
	 * @param user
	 *            user
	 * @param path
	 *            path in user repo
	 * @return true if exists.
	 */
	public boolean hasFileEntry(User user, String path) {
		return fileEntityRepository.hasFileEntry(user, path);
=======
	 *
	 * @param user user
	 * @param path path in user repo
	 * @return true if exists.
	 */
	public boolean hasFileEntry(User user, String path) {
		return fileEntityRepository.hasOne(user, path);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Add folder on the given path.
<<<<<<< HEAD
	 * 
	 * @param user
	 *            user
	 * @param path
	 *            base path
	 * @param comment
	 *            comment
	 * @param folderName
	 *            folder name
	 */
	public void addFolder(User user, String path, String folderName, String comment) {
		FileEntry entry = new FileEntry();
		entry.setPath(PathUtil.join(path, folderName));
=======
	 *
	 * @param user       user
	 * @param path       base path
	 * @param comment    comment
	 * @param folderName folder name
	 */
	public void addFolder(User user, String path, String folderName, String comment) {
		FileEntry entry = new FileEntry();
		entry.setPath(PathUtils.join(path, folderName));
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		entry.setFileType(FileType.DIR);
		entry.setDescription(comment);
		fileEntityRepository.save(user, entry, null);
	}

<<<<<<< HEAD
	/**
	 * Get file entity for the given revision.
	 * 
	 * @param user
	 *            the user
	 * @param path
	 *            path in the repo
	 * @param revision
	 *            revision. if -1, HEAD
	 * @return file entity
	 */
	public FileEntry getFileEntry(User user, String path, Long revision) {
		SVNRevision svnRev = (revision == null || revision == -1) ? SVNRevision.HEAD : SVNRevision.create(revision);
		return fileEntityRepository.findOne(user, path, svnRev);
	}

	/**
	 * Save File entry.
	 * 
	 * @param user
	 *            the user
	 * @param fileEntity
	 *            fileEntity to be saved
=======

	/**
	 * Save File entry.
	 *
	 * @param user       the user
	 * @param fileEntity fileEntity to be saved
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 */
	public void save(User user, FileEntry fileEntity) {
		prepare(user);
		checkNotEmpty(fileEntity.getPath());
		fileEntityRepository.save(user, fileEntity, fileEntity.getEncoding());
	}

	/**
	 * Delete file entries.
<<<<<<< HEAD
	 * 
	 * @param user
	 *            the user
	 * @param path
	 *            the base path
	 * @param files
	 *            files under base path
	 */
	public void delete(User user, String path, String[] files) {
		List<String> fileList = new ArrayList<String>();
		for (String each : files) {
			fileList.add(path + "/" + each);
		}
		fileEntityRepository.delete(user, fileList.toArray(new String[] {}));
=======
	 *
	 * @param user     the user
	 * @param basePath the base path
	 * @param files    files under base path
	 */
	public void delete(User user, String basePath, String[] files) {
		List<String> fullPathFiles = new ArrayList<String>();
		for (String each : files) {
			fullPathFiles.add(basePath + "/" + each);
		}
		fileEntityRepository.delete(user, fullPathFiles);
	}

	/**
	 * Delete file entry.
	 *
	 * @param user the user
	 * @param path the path
	 */
	public void delete(User user, String path) {
		fileEntityRepository.delete(user, newArrayList(path));
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	String getPathFromUrl(String urlString) {
		try {
			URL url = new URL(urlString);
			String urlPath = "/".equals(url.getPath()) ? "" : url.getPath();
<<<<<<< HEAD
			return (url.getHost() + urlPath).replaceAll("[\\&\\?\\%\\-]", "_");
=======
			return (url.getHost() + urlPath).replaceAll("[;\\&\\?\\%\\$\\-\\#]", "_");
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		} catch (MalformedURLException e) {
			throw processException("Error while translating " + urlString, e);
		}
	}

	String[] dividePathAndFile(String path) {
		int lastIndexOf = path.lastIndexOf("/");
		if (lastIndexOf == -1) {
<<<<<<< HEAD
			return new String[] { path, "" };
		} else {
			return new String[] { path.substring(0, lastIndexOf), path.substring(lastIndexOf + 1) };
=======
			return new String[]{"", path};
		} else {
			return new String[]{path.substring(0, lastIndexOf), path.substring(lastIndexOf + 1)};
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		}
	}

	/**
	 * Create new FileEntry.
<<<<<<< HEAD
	 * 
	 * @param user
	 *            user
	 * @param path
	 *            base path path
	 * @param fileName
	 *            fileName
	 * @param name
	 *            name
	 * @param url
	 *            url
	 * @param scriptHandler
	 *            script handler
	 * @param libAndResource
	 *            true if lib and resources should be created
	 * @return created file entry. main test file if it's the project creation.
	 */
	public FileEntry prepareNewEntry(User user, String path, String fileName, String name, String url,
			ScriptHandler scriptHandler, boolean libAndResource) {
=======
	 *
	 * @param user           user
	 * @param path           base path path
	 * @param fileName       fileName
	 * @param name           name
	 * @param url            url
	 * @param scriptHandler  script handler
	 * @param libAndResource true if lib and resources should be created
	 * @return created file entry. main test file if it's the project creation.
	 */
	public FileEntry prepareNewEntry(User user, String path, String fileName, String name, String url,
	                                 ScriptHandler scriptHandler, boolean libAndResource) {
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		if (scriptHandler instanceof ProjectHandler) {
			scriptHandler.prepareScriptEnv(user, path, fileName, name, url, libAndResource);
			return null;
		}
<<<<<<< HEAD
		path = PathUtil.join(path, fileName);
=======
		path = PathUtils.join(path, fileName);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		FileEntry fileEntry = new FileEntry();
		fileEntry.setPath(path);
		fileEntry.setContent(loadTemplate(user, scriptHandler, url, name));
		if (!"http://please_modify_this.com".equals(url)) {
			fileEntry.setProperties(buildMap("targetHosts", UrlUtils.getHost(url)));
		} else {
			fileEntry.setProperties(new HashMap<String, String>());
		}
		return fileEntry;
	}

	/**
	 * Create new FileEntry for the given URL.
<<<<<<< HEAD
	 * 
	 * @param user
	 *            user
	 * @param url
	 *            URL to be tested.
	 * @param scriptHandler
	 *            scriptHandler
=======
	 *
	 * @param user          user
	 * @param url           URL to be tested.
	 * @param scriptHandler scriptHandler
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return created new {@link FileEntry}
	 */
	public FileEntry prepareNewEntryForQuickTest(User user, String url, ScriptHandler scriptHandler) {
		String path = getPathFromUrl(url);
		String host = UrlUtils.getHost(url);
		FileEntry quickTestFile = scriptHandler.getDefaultQuickTestFilePath(path);
		if (scriptHandler instanceof ProjectHandler) {
			String[] pathPart = dividePathAndFile(path);
			prepareNewEntry(user, pathPart[0], pathPart[1], host, url, scriptHandler, false);
		} else {
			FileEntry fileEntry = prepareNewEntry(user, path, quickTestFile.getFileName(), host, url, scriptHandler,
					false);
			fileEntry.setDescription("Quick test for " + url);
			save(user, fileEntry);
		}
		return quickTestFile;
	}

	/**
	 * Load freemarker template for quick test.
<<<<<<< HEAD
	 * 
	 * @param user
	 *            user
	 * @param handler
	 *            handler
	 * @param url
	 *            url
	 * @param name
	 *            name
=======
	 *
	 * @param user    user
	 * @param handler handler
	 * @param url     url
	 * @param name    name
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return generated test script
	 */
	public String loadTemplate(User user, ScriptHandler handler, String url, String name) {
		Map<String, Object> map = newHashMap();
		map.put("url", url);
		map.put("userName", user.getUserName());
		map.put("name", name);
		return handler.getScriptTemplate(map);
	}

	/**
<<<<<<< HEAD
	 * Get SVN URL for the given user and the given subpath. Base path and the
	 * subpath is separated by ####.
	 * 
	 * @param user
	 *            user
	 * @param path
	 *            subpath
	 * @return SVN URL
	 */
	public String getSvnUrl(User user, String path) {
		String contextPath = getCurrentContextPathFromUserRequest();
		StringBuilder url = new StringBuilder(contextPath);
		url.append("/svn/").append(user.getUserId());
		if (StringUtils.isNotEmpty(path)) {
			url.append("/").append(path.trim());
		}
		return url.toString();
	}

	/**
	 * Get current context path url by user request.
	 * 
	 * @return context path
	 */
	public String getCurrentContextPathFromUserRequest() {
		return config.getSystemProperties().getProperty("http.url",
				httpContainerContext.getCurrentContextUrlFromUserRequest());
	}

	/**
	 * Get a SVN content into given dir.
	 * 
	 * @param user
	 *            user
	 * @param fromPath
	 *            path in svn subpath
	 * @param toDir
	 *            to directory
=======
	 * Get a SVN content into given dir.
	 *
	 * @param user     user
	 * @param fromPath path in svn subPath
	 * @param toDir    to directory
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 */
	public void writeContentTo(User user, String fromPath, File toDir) {
		fileEntityRepository.writeContentTo(user, fromPath, toDir);
	}

	/**
	 * Get the appropriate {@link ScriptHandler} subclass for the given
	 * {@link FileEntry}.
<<<<<<< HEAD
	 * 
	 * @param scriptEntry
	 *            script entry
=======
	 *
	 * @param scriptEntry script entry
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return scriptHandler
	 */
	public ScriptHandler getScriptHandler(FileEntry scriptEntry) {
		return scriptHandlerFactory.getHandler(scriptEntry);
	}

	/**
	 * Get the appropriate {@link ScriptHandler} subclass for the given
	 * ScriptHandler key.
<<<<<<< HEAD
	 * 
	 * @param key
	 *            script entry
=======
	 *
	 * @param key script entry
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return scriptHandler
	 */
	public ScriptHandler getScriptHandler(String key) {
		return scriptHandlerFactory.getHandler(key);
	}
}
