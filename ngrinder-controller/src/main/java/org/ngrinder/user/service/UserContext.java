/* 
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */
package org.ngrinder.user.service;

<<<<<<< HEAD
import org.ngrinder.infra.annotation.RuntimeOnlyComponent;
import org.ngrinder.model.User;
import org.ngrinder.security.SecuredUser;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * User Context which return current user.
 * 
=======
import org.ngrinder.model.User;
import org.ngrinder.security.SecuredUser;
import org.springframework.context.annotation.Profile;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

/**
 * User Context which return current user.
 *
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
 * @author Tobi
 * @author JunHo Yoon
 * @since 3.0
 */
<<<<<<< HEAD
@RuntimeOnlyComponent
=======
@Profile("production")
@Component
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
public class UserContext {


	/**
	 * Get current user object from context.
<<<<<<< HEAD
	 * 
=======
	 *
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return current user;
	 */
	public User getCurrentUser() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth == null) {
<<<<<<< HEAD
			throw new AuthenticationCredentialsNotFoundException("No athenticated");
		}
		Object obj = auth.getPrincipal();
		if (!(obj instanceof SecuredUser)) {
			throw new AuthenticationCredentialsNotFoundException("Invalid athentication with " +  obj);
=======
			throw new AuthenticationCredentialsNotFoundException("No authentication");
		}
		Object obj = auth.getPrincipal();
		if (!(obj instanceof SecuredUser)) {
			throw new AuthenticationCredentialsNotFoundException("Invalid authentication with " + obj);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		}
		SecuredUser securedUser = (SecuredUser) obj;
		return securedUser.getUser();
	}
}
