/* 
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */
package org.ngrinder.region.service;

<<<<<<< HEAD
import static org.ngrinder.common.util.ExceptionUtils.processException;

import java.io.File;
import java.util.HashSet;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import net.grinder.common.processidentity.AgentIdentity;
import net.grinder.util.thread.InterruptibleRunnable;
import net.sf.ehcache.Ehcache;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.ngrinder.infra.config.Config;
import org.ngrinder.infra.schedule.ScheduledTask;
import org.ngrinder.perftest.service.AgentManager;
import org.ngrinder.region.model.RegionInfo;
=======
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import net.grinder.common.processidentity.AgentIdentity;
import net.grinder.util.NetworkUtils;
import net.sf.ehcache.Ehcache;
import org.apache.commons.lang.StringUtils;
import org.ngrinder.common.constant.ClusterConstants;
import org.ngrinder.common.util.TypeConvertUtils;
import org.ngrinder.infra.config.Config;
import org.ngrinder.infra.schedule.ScheduledTaskService;
import org.ngrinder.perftest.service.AgentManager;
import org.ngrinder.region.model.RegionInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.Cache.ValueWrapper;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Service;

<<<<<<< HEAD
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

/**
 * Region service class. This class responsible to keep the status of available regions.
 * 
=======
import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;

import static org.ngrinder.common.util.ExceptionUtils.processException;

/**
 * Region service class. This class responsible to keep the status of available regions.
 *
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
 * @author Mavlarn
 * @author JunHo Yoon
 * @since 3.1
 */
@Service
public class RegionService {

<<<<<<< HEAD
=======
	@SuppressWarnings("UnusedDeclaration")
	private static final Logger LOGGER = LoggerFactory.getLogger(RegionService.class);

>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	@Autowired
	private Config config;

	@Autowired
<<<<<<< HEAD
	private ScheduledTask scheduledTask;
=======
	private ScheduledTaskService scheduledTaskService;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad

	@Autowired
	private CacheManager cacheManager;
	private Cache cache;

<<<<<<< HEAD
	/**
	 * Set current region into cache, using the IP as key and region name as value.
	 * 
	 */
	@PostConstruct
	public void initRegion() {
		if (config.isCluster()) {
			cache = cacheManager.getCache("regions");
			verifyDuplicateRegion();
			scheduledTask.addScheduledTaskEvery3Sec(new InterruptibleRunnable() {
				@Override
				public void interruptibleRun() {
					checkRegionUpdate();
				}

			});
=======

	/**
	 * Set current region into cache, using the IP as key and region name as value.
	 */
	@PostConstruct
	public void initRegion() {
		if (config.isClustered()) {
			cache = cacheManager.getCache("regions");
			verifyDuplicatedRegion();
			scheduledTaskService.addFixedDelayedScheduledTask(new Runnable() {
				@Override
				public void run() {
					checkRegionUpdate();
				}
			}, 3000);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		}
	}

	/**
	 * Verify duplicate region when starting with cluster mode.
<<<<<<< HEAD
	 * 
	 * @since 3.2
	 */
	private void verifyDuplicateRegion() {
		Map<String, RegionInfo> regions = getRegions();
		String localRegion = getCurrentRegion();
		RegionInfo regionInfo = regions.get(localRegion);
		if (regionInfo != null && !StringUtils.equals(regionInfo.getIp(), config.getCurrentIP())) {
			throw processException("The region name, " + localRegion
							+ ", is already used by other controller " + regionInfo.getIp()
							+ ". Please set the different region name in this controller.");
=======
	 *
	 * @since 3.2
	 */
	private void verifyDuplicatedRegion() {
		Map<String, RegionInfo> regions = getAll();
		String localRegion = getCurrent();
		RegionInfo regionInfo = regions.get(localRegion);
		if (regionInfo != null && !StringUtils.equals(regionInfo.getIp(), config.getClusterProperties().getProperty
				(ClusterConstants.PROP_CLUSTER_HOST, NetworkUtils.DEFAULT_LOCAL_HOST_ADDRESS))) {
			throw processException("The region name, " + localRegion
					+ ", is already used by other controller " + regionInfo.getIp()
					+ ". Please set the different region name in this controller.");
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		}
	}

	@Autowired
	private AgentManager agentManager;

	/**
	 * check Region and Update its value.
	 */
	public void checkRegionUpdate() {
		if (!config.isInvisibleRegion()) {
<<<<<<< HEAD
			HashSet<AgentIdentity> newHashSet = Sets.newHashSet(agentManager.getAllAttachedAgents());
			cache.put(getCurrentRegion(), new RegionInfo(config.getCurrentIP(), newHashSet));
		}
	}

	/**
	 * Destroy method. this method is responsible to delete our current region from dist cache.
	 */
	@PreDestroy
	public void destroy() {
		if (config.isCluster()) {
			File file = new File(config.getHome().getControllerShareDirectory(), config.getRegion());
			FileUtils.deleteQuietly(file);
		}
	}

	/**
	 * Get current region. This method returns where this service is running.
	 * 
	 * @return current region.
	 */
	public String getCurrentRegion() {
=======
			try {
				HashSet<AgentIdentity> newHashSet = Sets.newHashSet(agentManager.getAllAttachedAgents());
				final String regionIP = StringUtils.defaultIfBlank(config.getCurrentIP(), NetworkUtils.DEFAULT_LOCAL_HOST_ADDRESS);
				cache.put(getCurrent(), new RegionInfo(regionIP, config.getControllerPort(), newHashSet));
			} catch (Exception e) {
				LOGGER.error("Error while updating regions. {}", e.getMessage());
			}
		}
	}


	/**
	 * Get current region. This method returns where this service is running.
	 *
	 * @return current region.
	 */
	public String getCurrent() {
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		return config.getRegion();
	}

	/**
<<<<<<< HEAD
	 * Get region list of all clustered controller.
	 * 
	 * @return region list
	 */
	public Map<String, RegionInfo> getRegions() {
		Map<String, RegionInfo> regions = Maps.newHashMap();
		if (config.isCluster()) {
			for (Object eachKey : ((Ehcache) (cache.getNativeCache())).getKeys()) {
=======
	 * Get region by region name
	 *
	 * @param regionName
	 * @return region info
	 */
	public RegionInfo getOne(String regionName) {
		return (RegionInfo) cache.get(regionName).get();
	}

	/**
	 * Get region list of all clustered controller.
	 *
	 * @return region list
	 */
	public Map<String, RegionInfo> getAll() {
		Map<String, RegionInfo> regions = Maps.newHashMap();
		if (config.isClustered()) {
			for (Object eachKey : ((Ehcache) (cache.getNativeCache())).getKeysWithExpiryCheck()) {
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
				ValueWrapper valueWrapper = cache.get(eachKey);
				if (valueWrapper != null && valueWrapper.get() != null) {
					regions.put((String) eachKey, (RegionInfo) valueWrapper.get());
				}
			}
		}
		return regions;
	}

<<<<<<< HEAD
=======
	public ArrayList<String> getAllVisibleRegionNames() {
		final ArrayList<String> regions = new ArrayList<String>();
		if (config.isClustered()) {
			for (Object eachKey : ((Ehcache) (cache.getNativeCache())).getKeysWithExpiryCheck()) {
				ValueWrapper valueWrapper = cache.get(eachKey);
				if (valueWrapper != null && valueWrapper.get() != null) {
					final RegionInfo region = TypeConvertUtils.cast(valueWrapper.get());
					if (region.isVisible()) {
						regions.add((String) eachKey);
					}
				}
			}
		}
		Collections.sort(regions);
		return regions;
	}

>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	Config getConfig() {
		return config;
	}

<<<<<<< HEAD
	void setConfig(Config config) {
		this.config = config;
	}
=======
	/**
	 * For unit test
	 */
	public void setConfig(Config config) {
		this.config = config;
	}

	/**
	 * For unit test
	 */
	public void setCache(Cache cache) {
		this.cache = cache;
	}
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
}
