package net.grinder.engine.process;

import net.grinder.engine.process.GrinderProcess.ThreadContexts;

/**
 * ThreadContext updater in JUnit context.
 * 
 * This class is responsible to update the Grinder thread context values when it's not executed in
 * the Grinder agent.
 * 
 * @author JunHo Yoon
 * @since 3.2.1
 * 
 */
public class JUnitThreadContextUpdater {
	private ThreadContexts m_threadContexts;

	/**
	 * Constructor.
	 * 
<<<<<<< HEAD
	 * @param threadContexts
	 *            threadContexts
=======
	 * @param threadContexts	threadContexts
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 */
	public JUnitThreadContextUpdater(ThreadContexts threadContexts) {
		this.m_threadContexts = threadContexts;
	}

	/**
	 * Set run count in thread context.
	 * 
<<<<<<< HEAD
	 * @param count
	 *            count
=======
	 * @param count	count
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 */
	public void setRunCount(int count) {
		m_threadContexts.get().setCurrentRunNumber(count);
	}
	
}
